# Interactive map of Lyon

### Resto's à Lyon!

This is a web site project Resto's à Lyon! developed during a subject of 3rd year "LIFPROJET"  at l’Université Claude Bernard Lyon 1.  The main idea is to put into practice our team knowledge but also make us explore new technoly and tools of Web programming. We continue this project which was started by our friends Nelly BARRET and Louis LE BRUN.

### Prerequisites

You need an internet connection and also Node.js framework as well as MongoDB Databese installed on your computer.

You need to install MongoDB (https://docs.mongodb.com/manual/installation/) and NodeJS (https://nodejs.org/en/download/)

You can also install MongoDB Compass for a easier manage of your database in MongoDB

## Getting Started

First, you need to start MongoDB service:
   * In Linux, with the command following `sudo service mongod start`
   * In Windows, you need to go to the BIN directory where your MongoDB installed then type "mongod" or you can start this service in Task Manager

Then , to import the GEOJSON file into your MongoDB you should tap into an open Terminal tab:


`mongoimport --db NameOfDB  --collection NameOfCollection --file absolute_path_to_the_file.geojson`

If this doesn't work (most of the time in Windows), you should put your file geojson into the BIN directory where your MongoDB installed then use this command:


`mongoimport --db NameOfDB  --collection NameOfCollection --file your_file_geojson.geojson`

NB!!! To use the newest version of our collected data, take "newBD.geojson" that you can find in the ./public/JSON/newBD.geojson of our project.
Replace "NameOfDB" with the desired name for your databse in the upper import line as well as "NameOfCollection" for the desired name for your collection in the database.


Second, go to directory of this project then type the command below in your command terminal:
   
   `node server.js`
   
During development this can become a pain. To make this process easier install nodemon, a tool that will monitor your code for changes and automatically restart the server when necessary. To install nodemon:

  `npm install -g nodemon`

You can then run `nodemon server.js` to run your server and have it reloaded when you make changes. 

There is a example how NodeJS and MongoDB works (https://gist.github.com/aerrity/fd393e5511106420fba0c9602cc05d35) which, in our opinion, is simple to understand

### Running the tests

There is a debug mode for the data retrieve.

### Data
Example of place informations:
```
    {
        formatted_address       : "["Metro Cuire (C)","69300 Caluire et Cuire","France"]",
        formatted_phone_number  : "06 61 41 99 60",
        geometry                : "{"location":{"lat":45.785595167419004,"lng":4.83302194434442}}",
        mainType                : "Bar-Restaurant",
        name                    : "Pitakia",
        place_id                : "pitakia-caluire-et-cuire",
        price                   : "€",
        rating                  : 4,
        scope                   : "YELP",
        subtypes                : "[{"alias":"foodtrucks","title":"Food Trucks"},{"alias":"greek","title":"Greek"}]",
        url                     : "https://www.yelp.com/biz/pitakia-caluire-et-cuire?adjust_creative=tF3mc9kTCu1E1IXMV0XVwQ&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=tF3mc9kTCu1E1IXMV0XVwQ",
        vicinity                : "Metro Cuire (C)"
    }
```
### Built With

* [Mapbox](https://www.mapbox.com/) - API for map
* [Google Places](https://developers.google.com/places/) - API for data
* [Yelp](https://www.yelp.com/) - second API for data
* [Bootstrap](https://getbootstrap.com/) - Web framework
* [JsDoc](http://usejsdoc.org/) - API documentation generator for JavaScript (generate JsDoc (at the root of the project) : jsdoc js -d=doc)
* [NodeJS](http://https://nodejs.org/en) - Javascript Framework used for the server and the database
* [MongoDB](https://www.mongodb.com/fr)  - Document oriented database managment system


### Versioning

* [GitLab](https://about.gitlab.com/) for versioning.  

### Authors
- Fall 2018:
  * Andrew ALBERT
  * François ROBERT
  * Rodislav IVANOV
  * Nguyen NGUYEN
  
- Spring 2018:
  * Nelly BARRET
  * Louis LE BRUN

### License

This project is licensed under copyright.

COPYRIGHTS @ Andrew ALBERT, François ROBERT, Rodislav IVANOV, Nguyen NGUYEN, Nelly BARRET, Louis LE BRUN - LIFPROJET 2018
